#!/usr/bin/python3
#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Wrapper to list and handle system services
"""

import argparse
import os

from plinth import action_utils, cfg
from plinth.modules.i2p.helpers import RouterEditor, TunnelEditor

cfg.read()
module_config_path = os.path.join(cfg.config_dir, 'modules-enabled')

I2P_CONF_DIR = '/var/lib/i2p/i2p-config'


def parse_arguments():
    """Return parsed command line arguments as dictionary."""
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='subcommand', help='Sub command')

    subparsers.add_parser('enable', help='enable i2p service')
    subparsers.add_parser('disable', help='disable i2p service')

    subparser = subparsers.add_parser(
        'add-favorite', help='Add an eepsite to the list of favorites')
    subparser.add_argument('--name', help='Name of the entry', required=True)
    subparser.add_argument('--url', help='URL of the entry', required=True)
    subparser.add_argument('--description', help='Short description',
                           required=False)
    subparser.add_argument('--icon', help='URL to icon', required=False)

    subparser = subparsers.add_parser('set-tunnel-property',
                                      help='Modify configuration of a tunnel')
    subparser.add_argument('--name', help='Name of the tunnel', required=True)
    subparser.add_argument('--property', help='Property to modify',
                           required=True)
    subparser.add_argument('--value', help='Value to assign', required=True)

    subparsers.required = True
    return parser.parse_args()


def subcommand_enable(_):
    """Enable I2P service."""
    action_utils.service_enable('i2p')
    action_utils.webserver_enable('i2p-freedombox')


def subcommand_disable(_):
    """Disable I2P service."""
    action_utils.service_disable('i2p')
    action_utils.webserver_disable('i2p-freedombox')


def subcommand_set_tunnel_property(arguments):
    """Modify the configuration file for a certain tunnel."""
    editor = TunnelEditor()
    editor \
        .read_conf() \
        .set_tunnel_idx(arguments.name) \
        .set_tunnel_prop(arguments.property, arguments.value) \
        .write_conf()
    print('Updated "{property}" of {filename} to {value}'.format(
        property=editor.calc_prop_path(arguments.property),
        filename=editor.conf_filename, value=arguments.value))


def subcommand_add_favorite(arguments):
    """
    Adds a favorite to the router.config

    :param arguments:
    :type arguments:
    """
    url = arguments.url

    editor = RouterEditor()
    editor.read_conf().add_favorite(arguments.name, url, arguments.description,
                                    arguments.icon).write_conf()

    print('Added {} to favorites'.format(url))


def main():
    """Parse arguments and perform all duties."""
    arguments = parse_arguments()

    subcommand = arguments.subcommand.replace('-', '_')
    subcommand_method = globals()['subcommand_' + subcommand]
    subcommand_method(arguments)


if __name__ == '__main__':
    main()
